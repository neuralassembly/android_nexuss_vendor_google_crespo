#!/bin/sh
if [ $# -ne 2 ]; then
   echo "example usage: $0 \$ANDROID_ROOT ~/crespo_binaries"
   exit 1
fi

JCROM_ANDROID_ROOT=$1
JCROM_CRESPO_BIN=$2

mkdir -p ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/etc/updatecmds
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/etc/permissions
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/video
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/framework
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/vendor/firmware
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/vendor/lib/drm

cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/etc/updatecmds/google_generic_update.txt ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/etc/updatecmds/google_generic_update.txt
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/etc/permissions/com.google.android.media.effects.xml ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/etc/permissions/com.google.android.media.effects.xml
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/etc/permissions/features.xml ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/etc/permissions/features.xml
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/etc/permissions/com.google.android.maps.xml ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/etc/permissions/com.google.android.maps.xml
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/bootanimation.zip ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/bootanimation.zip
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/PFFprec_600.emd ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/PFFprec_600.emd
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/LMprec_508.emd ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/LMprec_508.emd
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/video/Sunset.240p.mp4 ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/video/Sunset.240p.mp4
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/video/AndroidInSpace.240p.mp4 ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/video/AndroidInSpace.240p.mp4
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/video/AndroidInSpace.480p.mp4 ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/video/AndroidInSpace.480p.mp4
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/video/Sunset.480p.mp4 ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/video/Sunset.480p.mp4
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/video/Disco.240p.mp4 ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/video/Disco.240p.mp4
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/media/video/Disco.480p.mp4 ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/media/video/Disco.480p.mp4
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/framework/com.google.android.media.effects.jar ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/framework/com.google.android.media.effects.jar
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/framework/com.google.android.maps.jar ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/framework/com.google.android.maps.jar
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/framework/com.google.widevine.software.drm.jar ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/framework/com.google.widevine.software.drm.jar
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Microbes/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Microbes/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Microbes/Microbes.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Microbes/Microbes.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GalleryGoogle/GalleryGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GalleryGoogle/GalleryGoogle.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GalleryGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GalleryGoogle/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GenieWidget/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GenieWidget/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GenieWidget/GenieWidget.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GenieWidget/GenieWidget.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleLoginService/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleLoginService/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleLoginService/GoogleLoginService.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleLoginService/GoogleLoginService.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/VideoEditorGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/VideoEditorGoogle/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/VideoEditorGoogle/VideoEditorGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/VideoEditorGoogle/VideoEditorGoogle.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/CalendarGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/CalendarGoogle/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/CalendarGoogle/CalendarGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/CalendarGoogle/CalendarGoogle.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/OneTimeInitializer/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/OneTimeInitializer/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/OneTimeInitializer/OneTimeInitializer.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/OneTimeInitializer/OneTimeInitializer.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/SetupWizard/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/SetupWizard/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/SetupWizard/SetupWizard.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/SetupWizard/SetupWizard.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/talkback/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/talkback/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/talkback/talkback.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/talkback/talkback.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleContactsSyncAdapter/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleContactsSyncAdapter/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/TagGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/TagGoogle/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/TagGoogle/TagGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/TagGoogle/TagGoogle.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleEarth/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleEarth/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleEarth/GoogleEarth.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleEarth/GoogleEarth.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Music2/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Music2/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Music2/Music2.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Music2/Music2.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/ChromeBookmarksSyncAdapter/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/ChromeBookmarksSyncAdapter/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/ChromeBookmarksSyncAdapter/ChromeBookmarksSyncAdapter.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/ChromeBookmarksSyncAdapter/ChromeBookmarksSyncAdapter.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/googlevoice/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/googlevoice/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/googlevoice/googlevoice.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/googlevoice/googlevoice.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleFeedback/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleFeedback/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleFeedback/GoogleFeedback.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleFeedback/GoogleFeedback.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/MediaUploader/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/MediaUploader/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/MediaUploader/MediaUploader.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/MediaUploader/MediaUploader.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Talk/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Talk/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Talk/Talk.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Talk/Talk.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleQuickSearchBox/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleQuickSearchBox/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleQuickSearchBox/GoogleQuickSearchBox.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleQuickSearchBox/GoogleQuickSearchBox.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/VoiceSearch/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/VoiceSearch/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/VoiceSearch/VoiceSearch.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/VoiceSearch/VoiceSearch.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Gmail/Gmail.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Gmail/Gmail.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Gmail/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Gmail/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/ExchangeGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/ExchangeGoogle/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/ExchangeGoogle/ExchangeGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/ExchangeGoogle/ExchangeGoogle.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Maps/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Maps/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Maps/Maps.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Maps/Maps.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Thinkfree/Thinkfree.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Thinkfree/Thinkfree.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Thinkfree/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Thinkfree/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/BooksTablet/BooksTablet.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/BooksTablet/BooksTablet.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/BooksTablet/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/BooksTablet/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/DeskClockGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/DeskClockGoogle/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/DeskClockGoogle/DeskClockGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/DeskClockGoogle/DeskClockGoogle.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Phonesky/Phonesky.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Phonesky/Phonesky.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Phonesky/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Phonesky/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/NetworkLocation/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/NetworkLocation/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/NetworkLocation/NetworkLocation.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/NetworkLocation/NetworkLocation.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/PlusOne/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/PlusOne/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/PlusOne/PlusOne.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/PlusOne/PlusOne.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/CarHomeGoogle/CarHomeGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/CarHomeGoogle/CarHomeGoogle.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/CarHomeGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/CarHomeGoogle/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleBackupTransport/GoogleBackupTransport.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleBackupTransport/GoogleBackupTransport.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleBackupTransport/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleBackupTransport/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GooglePartnerSetup/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GooglePartnerSetup/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GooglePartnerSetup/GooglePartnerSetup.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GooglePartnerSetup/GooglePartnerSetup.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleServicesFramework/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleServicesFramework/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleServicesFramework/GoogleServicesFramework.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleServicesFramework/GoogleServicesFramework.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Videos/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Videos/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Videos/Videos.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Videos/Videos.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Street/Street.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Street/Street.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/Street/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/Street/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/EmailGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/EmailGoogle/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/EmailGoogle/EmailGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/EmailGoogle/EmailGoogle.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/YouTube/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/YouTube/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/YouTube/YouTube.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/YouTube/YouTube.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleTTS/GoogleTTS.apk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleTTS/GoogleTTS.apk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/app/GoogleTTS/Android.mk ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/GoogleTTS/Android.mk
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libgcomm_jni.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libgcomm_jni.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libext2fs.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libext2fs.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libext2_uuid.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libext2_uuid.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libvoicesearch.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libvoicesearch.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libext2_profile.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libext2_profile.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libfilterpack_facedetect.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libfilterpack_facedetect.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libflint_engine_jni_api.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libflint_engine_jni_api.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libearthmobile.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libearthmobile.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libfrsdk.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libfrsdk.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libpicowrapper.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libpicowrapper.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libspeexwrapper.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libspeexwrapper.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libWVphoneAPI.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libWVphoneAPI.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libext2_blkid.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libext2_blkid.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libvideochat_stabilize.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libvideochat_stabilize.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libmicrobes_jni.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libmicrobes_jni.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libext2_e2p.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libext2_e2p.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libext2_com_err.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libext2_com_err.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/lib/libvideochat_jni.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/lib/libvideochat_jni.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/vendor/firmware/cypress-touchkey.bin ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/vendor/firmware/cypress-touchkey.bin
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/vendor/lib/libwvdrm_L3.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/vendor/lib/libwvdrm_L3.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/vendor/lib/libWVStreamControlAPI_L3.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/vendor/lib/libWVStreamControlAPI_L3.so
cp -a ${JCROM_CRESPO_BIN}/google/crespo/proprietary/system/vendor/lib/libwvm.so ${JCROM_ANDROID_ROOT}/vendor/google/crespo/proprietary/system/vendor/lib/libwvm.so

