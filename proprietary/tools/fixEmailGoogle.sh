#!/bin/bash

TMP=EmailGoogle.tmp

grep -v \"main_content\"\" apk.out/res/values/ids.xml > ${TMP}
mv ${TMP} apk.out/res/values/ids.xml

grep -v \"main_content\"\" apk.out/res/values/public.xml > ${TMP}
mv ${TMP} apk.out/res/values/public.xml

cat apk.out/res/values-sw600dp/styles.xml | sed -e s/main_content\"/main_content/ > ${TMP}
mv ${TMP} apk.out/res/values-sw600dp/styles.xml

cat apk.out/res/values-sw800dp-port/styles.xml | sed -e s/main_content\"/main_content/ > ${TMP}
mv ${TMP} apk.out/res/values-sw800dp-port/styles.xml
