#!/bin/sh

echo Deodexing 26 files. This may take about 10 minutes.

bash ./make.sh  > log.txt 2>&1

mkdir ${ANDROID_ROOT}/vendor/google/crespo/proprietary/system/framework

cp -a ./com.google.widevine.software.drm.jar ${ANDROID_ROOT}/vendor/google/crespo/proprietary/system/framework/com.google.widevine.software.drm.jar
rm ./com.google.widevine.software.drm.jar

cp -a ./com.google.android.maps.jar ${ANDROID_ROOT}/vendor/google/crespo/proprietary/system/framework/com.google.android.maps.jar
rm ./com.google.android.maps.jar

cp -a ./com.google.android.media.effects.jar ${ANDROID_ROOT}/vendor/google/crespo/proprietary/system/framework/com.google.android.media.effects.jar
rm ./com.google.android.media.effects.jar

APPS="CalendarGoogle CarHomeGoogle ChromeBookmarksSyncAdapter DeskClockGoogle EmailGoogle ExchangeGoogle GalleryGoogle GenieWidget Gmail GoogleBackupTransport GoogleContactsSyncAdapter GoogleFeedback GoogleLoginService GooglePartnerSetup GoogleQuickSearchBox GoogleServicesFramework GoogleTTS Microbes NetworkLocation OneTimeInitializer SetupWizard TagGoogle Talk"

for APP in ${APPS}; do
 
cp -a ./${APP}.apk ${ANDROID_ROOT}/vendor/google/crespo/proprietary/system/app/${APP}/${APP}.apk
rm ./${APP}.apk

done