#!/bin/sh
if [ $# -ne 2 ]; then
   echo "example usage: $0 \$ANDROID_ROOT ~/nexuss_binaries"
   exit 1
fi

JCROM_ANDROID_ROOT=$1
JCROM_NEXUSS_BIN=$2

mkdir -p ${JCROM_NEXUSS_BIN}/akm/crespo/proprietary
mkdir -p ${JCROM_NEXUSS_BIN}/broadcom/crespo/proprietary
mkdir -p ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary
mkdir -p ${JCROM_NEXUSS_BIN}/nxp/crespo/proprietary
mkdir -p ${JCROM_NEXUSS_BIN}/samsung/crespo/proprietary

cp -a ${JCROM_ANDROID_ROOT}/vendor/akm/crespo/BoardConfigCrespo.mk ${JCROM_NEXUSS_BIN}/akm/crespo/BoardConfigCrespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/akm/crespo/device-crespo.mk ${JCROM_NEXUSS_BIN}/akm/crespo/device-crespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/akm/crespo/proprietary/libakm.so ${JCROM_NEXUSS_BIN}/akm/crespo/proprietary/libakm.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/broadcom/crespo/BoardConfigCrespo.mk ${JCROM_NEXUSS_BIN}/broadcom/crespo/BoardConfigCrespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/broadcom/crespo/device-crespo.mk ${JCROM_NEXUSS_BIN}/broadcom/crespo/device-crespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/broadcom/crespo/proprietary/gpsd ${JCROM_NEXUSS_BIN}/broadcom/crespo/proprietary/gpsd
cp -a ${JCROM_ANDROID_ROOT}/vendor/broadcom/crespo/proprietary/bcm4329.hcd ${JCROM_NEXUSS_BIN}/broadcom/crespo/proprietary/bcm4329.hcd
cp -a ${JCROM_ANDROID_ROOT}/vendor/broadcom/crespo/proprietary/gps.s5pc110.so ${JCROM_NEXUSS_BIN}/broadcom/crespo/proprietary/gps.s5pc110.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/BoardConfigCrespo.mk ${JCROM_NEXUSS_BIN}/imgtec/crespo/BoardConfigCrespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/device-crespo.mk ${JCROM_NEXUSS_BIN}/imgtec/crespo/device-crespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libpvrANDROID_WSEGL.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libpvrANDROID_WSEGL.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libEGL_POWERVR_SGX540_120.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libEGL_POWERVR_SGX540_120.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libGLESv2_POWERVR_SGX540_120.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libGLESv2_POWERVR_SGX540_120.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libPVRScopeServices.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libPVRScopeServices.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libsrv_init.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libsrv_init.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/gralloc.s5pc110.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/gralloc.s5pc110.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/pvrsrvinit ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/pvrsrvinit
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libusc.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libusc.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libIMGegl.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libIMGegl.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libsrv_um.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libsrv_um.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libglslcompiler.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libglslcompiler.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libGLESv1_CM_POWERVR_SGX540_120.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libGLESv1_CM_POWERVR_SGX540_120.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/crespo/proprietary/libpvr2d.so ${JCROM_NEXUSS_BIN}/imgtec/crespo/proprietary/libpvr2d.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/nxp/crespo/BoardConfigCrespo.mk ${JCROM_NEXUSS_BIN}/nxp/crespo/BoardConfigCrespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/nxp/crespo/device-crespo.mk ${JCROM_NEXUSS_BIN}/nxp/crespo/device-crespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/nxp/crespo/proprietary/libpn544_fw.so ${JCROM_NEXUSS_BIN}/nxp/crespo/proprietary/libpn544_fw.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/crespo/BoardConfigVendor.mk ${JCROM_NEXUSS_BIN}/samsung/crespo/BoardConfigVendor.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/crespo/BoardConfigCrespo.mk ${JCROM_NEXUSS_BIN}/samsung/crespo/BoardConfigCrespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/crespo/device-crespo.mk ${JCROM_NEXUSS_BIN}/samsung/crespo/device-crespo.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/crespo/proprietary/libsecril-client.so ${JCROM_NEXUSS_BIN}/samsung/crespo/proprietary/libsecril-client.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/crespo/proprietary/libsec-ril.so ${JCROM_NEXUSS_BIN}/samsung/crespo/proprietary/libsec-ril.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/crespo/device-vendor.mk ${JCROM_NEXUSS_BIN}/samsung/crespo/device-vendor.mk

